import React from 'react'
import './desktopLibrary.css'
import data from '../../data/data.json';

const gridCard = data.DesktopLibrary;

export default function DesktopLibrary() {
    return (
      <section className="destop-library">
        <div className="destop-library-wrapper">

          { gridCard.map((data, i) => (
            <div key ={i} className="destop-library-container">
              <div className="main-iamge-container">
                <img src={`${process.env.PUBLIC_URL}${data.mainImage}`} alt=""/>
              </div>

              <div className="body-container">
                <div className="left-body-container">

                  <h6>{data.title}</h6>

                  <div className="icon-image"> 
                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon-timer-line.png`} alt="" /> 
                    <span>{data.time}</span> 
                    <img src={`${process.env.PUBLIC_URL}/assets/images/icon-distance-line.png`} alt="" />
                    <span>{data.distance}</span>
                  </div>

                  <h5>VIEW DETAILS</h5>
                </div>

                <div className="right-body-container">
                  <img src={`${process.env.PUBLIC_URL}${data.trainerImage}`} alt="" className="img-responsive"/>
                </div>
              </div>
            </div>
          ))}
        </div>
      </section>
    )
}
