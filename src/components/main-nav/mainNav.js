import React from 'react'
import './mainNav.css'


export default function MainNav() {
    return (    
        <section className="main-nav">

            <div className="ifit-logo">
                <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/ifit-coach-logo.svg`} alt="boohoo" className="img-responsive"/>  
            </div>

            <div className="main-nav-wrapper">

                <div className="main-nav-container1">
                    <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/ifit-coach-logo.svg`} alt="boohoo" className="img-responsive"/>  
                </div>

                <div className="main-nav-container2">
                    <div>EXERCISE</div>
                    <div>NUTRITION</div>
                    <div>ACTIVITY</div>
                    <div>SLEEP</div>
                </div>

                <div className="main-nav-container3">
                    <button>SIGN UP</button>
                </div>
            </div>
        </section>
    )
}
