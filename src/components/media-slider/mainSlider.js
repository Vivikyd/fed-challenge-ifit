import React from 'react'
import './mainSlider.css'
import data from '../../data/data.json'

const ReviewSlider = data.ReviewSlider;

export default function MainSlider() {
    return (
        <section className="main-slider">
            <div className="main-slider-wrapper">
                { ReviewSlider.map((data, i) => (
                    <div key ={i} className="main-slider-content">
                        <div> 
                            <img src={`${process.env.PUBLIC_URL}${data.sliderImage}`} alt="boohoo" className="img-responsive"/>
                        </div>
                        <div>
                            <p>{data.sliderReview}</p>
                        </div>
                    </div>
                ))}
            </div>
        </section>
    )
}


