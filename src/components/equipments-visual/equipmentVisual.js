import React from 'react'
import './equipmentVisual.css'
import data from '../../data/data.json'

const equipment = data.EquipmentVisual;
export default function EquipmentVisual() {
    return (
        <section className="equipment-visual">
            <div className="equipment-visual-heading">
                <h4>Interested in our exciting iFit-enabled equipment?</h4>
            </div>
            
            <div className="equipment-visual-wrapper">
                {equipment.map((data, i) => (
                    <div key ={i} className="equipment-visual-content">
                        <img src={`${process.env.PUBLIC_URL}${data.equpimentImage}`} alt={data.equipmentName} />
                        <h4>{data.equipmentName}</h4>
                    </div>   
                ))}
            </div>

        </section>
    )
}
