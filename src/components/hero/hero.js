import React from 'react';
import './hero.css';

export default function Hero() {
    return (
        <section className="hero-container">
            <div className="hero-wrapper">
                <div className="hero-container-content">
                <h1>The best personal training, right in your own home.</h1>
                <button> JOIN IFIT COACH</button>
                </div>
            </div>
        </section>
    )
}
