import React from 'react'
import './productNav.css'

export default function productNav() {
    return (
        <section className="product-nav">
            <div><p>BLOG</p></div>
            <div><p>NOURISH</p></div>
            <div><p>SHOP</p></div>

        </section> 
    )
}
