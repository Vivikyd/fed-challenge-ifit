import React from 'react'
import './footer.css'
import data from '../../data/data.json'

const Language = data.Language;
export default function Footer(props) {
    return (
        <section className="footer">
            <div className="footer-box1">
                <div>
                    <ul>
                    <li>Company</li>
                    <li>About</li>
                    <li>Contact Us</li>
                    <li>Careers</li>
                    </ul>
                </div>

                <div>
                <ul>
                    <li>Account</li>
                    <li>Log in</li>
                    <li>Create Account</li>
                    </ul>
                </div>

                <div>
                <ul>
                    <li>Support</li>
                    <li>Help Center</li>
                    <li>Accessability</li>
                    </ul>
                </div>
            </div>

            <div className="footer-box2">
            <div className="footer-box2-wrapper">        
                <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/youtube.svg`} alt="" className="img-responsive"/>
                <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/pinterest.svg`} alt="" className="img-responsive"/>
                <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/facebook.svg`} alt="" className="img-responsive"/>
                <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/twitter.svg`} alt="" className="img-responsive"/>
                <img src={`${process.env.PUBLIC_URL}/assets/svg-logos/instagram.svg`} alt="" className="img-responsive"/>
            </div>
            </div> 
            <div className="footer-box3">
            <div>
                <select value={props.language}>
                { Language.map((data, i) => (
                    <option>{data.language}</option>
                ))}
                </select>
            </div>
            
            <div className="footer-final-list">
                <ul>
                <li><p> © iFit.com. All Rights Reserved</p></li>
                <li><p>Privacy Policy</p></li>
                <li><p>Terms of Use</p></li>
                </ul>
            </div>
            
            </div>
        </section>
    )
}
