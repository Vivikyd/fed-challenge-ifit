import React from 'react';
import './App.css';
import DesktopLibrary from './components/desktop-library/desktopLibrary';
import EquipmentVisual from './components/equipments-visual/equipmentVisual';
import Footer from './components/footer/footer';
import Hero from './components/hero/hero';
import MainNav from './components/main-nav/mainNav';
import MediaSilder from './components/media-slider/mainSlider';
import ProductNav from './components/product-nav/productNav';

function App() {
  return (

    <div className="App">
      <ProductNav />
      <MainNav />
      <Hero />
      <MediaSilder />
      <DesktopLibrary />
      <EquipmentVisual />
      <Footer />
    </div>
  );
}
export default App;
